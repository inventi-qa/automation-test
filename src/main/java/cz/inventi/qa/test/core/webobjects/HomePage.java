package cz.inventi.qa.test.core.webobjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage extends BasePage {

    @FindBy(xpath = "//div[contains(@class, 'toolTipImage')]")
    WebElement tooltipImg;
    @FindBy(xpath = "//div[contains(@class, 'scrollable')]")
    WebElement scrollableElement;
    @FindBy(xpath = "//span[contains(@class, 'toolTipText')]")
    WebElement toolTipText;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage assertPageLoaded () {
        Assert.assertTrue(scrollableElement.isDisplayed());
        return this;
    }
}
