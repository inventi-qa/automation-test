package cz.inventi.qa.test.core.components;

import cz.inventi.qa.test.fwmock.objects.WebObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactForm extends WebObject {

    @FindBy(xpath = "//input[1]")
    WebElement contactFormEmailInput;

    public ContactForm(WebDriver driver) {
        super(driver);
    }

    public WebElement getContactFormEmailInput() {
        return contactFormEmailInput;
    }
}
