package cz.inventi.qa.test.core.webobjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class WhatWeDoPage extends BasePage {

    @FindBy(xpath = "//table[@id='careerOptions']")
    WebElement careerOptionsTable;
    @FindBy(xpath = "//button[@id='appendSomeContent']")
    WebElement appendContentBtn;
    @FindBy(xpath = "//button[@id='refreshSomeContent']")
    WebElement refreshContentBtn;
    @FindBy(xpath = "//div[@id='appendedContent']")
    WebElement appendedContent;

    public WhatWeDoPage(WebDriver driver) {
        super(driver);
    }

    public WhatWeDoPage assertPageLoaded () {
        Assert.assertTrue(careerOptionsTable.isDisplayed());
        return this;
    }

    public WebElement getCareerOptionsTable() {
        return careerOptionsTable;
    }

    public WebElement getAppendContentBtn() {
        return appendContentBtn;
    }

    public WebElement getRefreshContentBtn() {
        return refreshContentBtn;
    }

    public WebElement getAppendedContent() {
        return appendedContent;
    }
}
