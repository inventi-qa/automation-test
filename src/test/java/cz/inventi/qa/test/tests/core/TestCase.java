package cz.inventi.qa.test.tests.core;


import cz.inventi.qa.test.core.webobjects.HomePage;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;

public class TestCase {
    public HomePage homePage;

    @BeforeClass
    public void init() {
        homePage = new HomePage(new ChromeDriver());
    }
}
